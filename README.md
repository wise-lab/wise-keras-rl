# wise-keras-rl

This repo is for self-upgrading keras-rl which is not actively supported anymore. Specifically, we plan to bug-fix the code, add features, and make it compatible with the up-to-date keras that supports tensorflow >= 2.2.